Name: Fastly real-time streaming log V2 Log format

Schema Data Description:

https://docs.fastly.com/en/guides/custom-log-formats

Notes:
See docs.hydrolix.io and docs.fastly.com for more information on how to set-up logging import with Hydrolix.
