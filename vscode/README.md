# This repo is hosting example for VSCode

## To use those example
Install the plugin [REST](https://marketplace.visualstudio.com/items?itemName=humao.rest-client)

You can refer to the following blog post for example:
https://www.hydrolix.io/blog/using-hydrolix-with-vscode/
