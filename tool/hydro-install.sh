#!/bin/bash
### Default variable
HKT=v3.16.5

#####################################
########## Common Function ##########
#####################################
create_namespace() {
  echo "============================================="
  echo "   Create Kubernetes namespace $CLIENT_ID    "
  echo "============================================="
  kubectl create namespace $CLIENT_ID
  kubectl config set-context --current --namespace="$CLIENT_ID"
  return
}

create_hydrolix_cluster() {
  CLOUD=$1
  echo "============================================="
  echo "           Create Hydrolix cluster           "
  echo "============================================="
  if [ ${CLOUD} == 'aws' ]
  then
    ./hkt operator-resources --bucket ${CLIENT_ID} --namespace ${CLIENT_ID} --aws-storage-role ${AWS_STORAGE_ROLE} > operator.yaml && kubectl apply -f operator.yaml
  elif [ ${CLOUD} == 'gcp' ]
  then
    ./hkt operator-resources --bucket ${CLIENT_ID} --namespace ${CLIENT_ID} --gcp-storage-sa ${GCP_STORAGE_SA} > operator.yaml && kubectl apply -f operator.yaml
  fi
  sleep 5
  ./hkt hydrolix-cluster --bucket ${CLIENT_ID} --namespace ${CLIENT_ID} --admin-email ${ADMIN_EMAIL} --host ${HYDROLIX_HOST} --domain ${HYDROLIX_DOMAIN} --owner ${OWNER} --cloud ${CLOUD} --region ${REGION} --ip-allowlist `curl -s ifconfig.me`/32 --scale-profile ${SCALE} > hydrolixcluster.yaml && kubectl apply -f hydrolixcluster.yaml
}

deployment_done() {
  CLOUD=$1  
  echo "============================================="
  echo "      Deployment done, your are all set!     "
  echo "============================================="
  echo "Please create the following DNS entry:"
  if [ ${CLOUD} == 'aws' ]
  then
    echo "$HYDROLIX_HOST.$HYDROLIX_DOMAIN   IN CNAME  $AWSELB"
    echo "Your bucket and namespace are ${CLIENT_ID}"
    echo "Your service account is: ${AWS_STORAGE_ROLE}"
    echo "Keep the following env variable: "
    echo "export HYDROLIX_NAMESPACE=${CLIENT_ID}"
    echo "export HYDROLIX_DB_BUCKET=${CLIENT_ID}"
    echo "export AWS_STORAGE_ROLE=${AWS_STORAGE_ROLE}"
  elif [ ${CLOUD} == 'gcp' ]
  then
    echo "$HYDROLIX_HOST.$HYDROLIX_DOMAIN   IN A  $IP"
    echo "Your bucket and namespace are ${CLIENT_ID}"
    echo "Your service account is: ${GCP_STORAGE_SA}"
    echo "Keep the following env variable: "
    echo "export HYDROLIX_NAMESPACE=${CLIENT_ID}"
    echo "export HYDROLIX_DB_BUCKET=${CLIENT_ID}"
    echo "export GCP_STORAGE_SA=${GCP_STORAGE_SA}"
  fi
  echo "You should have received an email to login"
  echo "Please check your spam just in case"
  exit
}

scale() {
  echo "Select your scale profile (default prod): "
  echo "Description of the scale profile: "
  echo "https://docs.hydrolix.io/docs/scale-profiles"
    options_scale=("minimal" "dev" "prod" "mega")
    select opt in "${options_scale[@]}"; do
    case $opt in

    "minimal")
        SCALE="minimal"
        AWSINSTANCETYPE="t3.2xlarge"
        GCPINSTANCETYPE="n2-standard-8"
        AWSCAPACITY=3
        return
        ;;
    "dev")
        SCALE="dev"
        AWSINSTANCETYPE="t3.2xlarge"
        GCPINSTANCETYPE="n2-standard-8"
        AWSCAPACITY=3
        return
        ;;
    "prod")
        SCALE="prod"
        AWSINSTANCETYPE="m5n.4xlarge"
        GCPINSTANCETYPE="n2-standard-16"
        AWSCAPACITY=7
        return
        ;;
    "mega")
        SCALE="mega"
        AWSINSTANCETYPE="m5n.4xlarge"
        GCPINSTANCETYPE="n2-standard-16"
        AWSCAPACITY=10
        return
        ;;
    esac
  done
}

get_hydrolix_cluster_info() {
  echo "Generating new client ID"
  CLIENT_ID="hdxcli-"`openssl rand -hex 4`
  echo "======================================="
  echo "Please configure the following settings"
  echo "======================================="
  read -r -e -p "Email adress for user: " ADMIN_EMAIL
  read -r -e -p "Name for admin (default admin): " OWNER
  OWNER="${OWNER:=admin}"
  printf "Hostname for Hydrolix cluster (example https://\e[1;33;4;44mhydrolix\e[0m.company.net)"
  read -r -e -p ": " HYDROLIX_HOST
  printf "Domain for Hydrolix cluster (example https://hydrolix.\e[1;33;4;44mcompany.net\e[0m)"
  read -r -e -p ": " HYDROLIX_DOMAIN
  return
}

download_hkt() {
  echo "============================================="
  echo " Downloading Hydrolix Kubernetes Tools $HKT  "
  echo "============================================="
  curl -o hkt -s https://hdx-infrastructure.s3.amazonaws.com/hkt/hkt-$HKT
  chmod +x hkt
  return
}
################ END ################
########## Common Function ##########
#####################################


######################################
##### Specific GKE Function ##########
######################################

verify_gke_right() {
  echo "======================================="
  echo "            Verifying GKE Rights       "
  echo "======================================="
  echo "Verifying user has proper permission"
  LIST_SERVICE=('autoscaling.googleapis.com' 'compute.googleapis.com' 'container.googleapis.com' 'iam.googleapis.com' 'iamcredentials.googleapis.com' 'pubsub.googleapis.com' 'storage-component.googleapis.com' 'storage.googleapis.com')
  GKE_SERVICES=`gcloud services list | cut -d " " -f 1  | grep -v NAME`
    for SERVICE in "${LIST_SERVICE[@]}"
      do
        GREP_RESULT="$(echo $GKE_SERVICES | grep $SERVICE)"
        if [ -z "$GREP_RESULT" ]
          then
          echo "==========================================="
          echo "           Installation canceled           "
          echo "==========================================="
          echo "$SERVICE is not enabled for your account on the project $PROJECT_ID"
          exit
        fi
    done
}

select_gke_region() {
  echo "Select region (default us-central1): "
  PS3="Enter a number: "
  select opt in `gcloud compute regions list | cut -d " " -f 1 | grep -v NAME`;
    do
      REGION=$opt
      REGION="${REGION:=us-central1}"
      return
    done
}

verify_command_gke() {
  if ! command -v $1 &> /dev/null
  then
    echo "$1 could not be found"
    echo "Installation canceled"
    echo "Please follow this guide to setup your environment"
    echo "https://cloud.google.com/kubernetes-engine/docs/how-to/cluster-access-for-kubectl"
    exit
  fi
}

kubectl_loop_gke() {
  IP=`kubectl get services -o yaml | grep -A 1 "ingress:" | grep ip | cut -d ":" -f 2 | cut -d " " -f 2`
  if [ -z "$IP" ]
  then
      echo "==========================================="
      echo "Creation of Hydrolix deployment in progress"
      echo "==========================================="
      sleep 30
      kubectl_loop_gke
  elif [ "$IP" == "<pending>" ]
  then
      echo "Creation of Hydrolix deployment in progress"
      sleep 30
      kubectl_loop_gke
  else
      return
fi
}

create_gke_project() {
  echo "======================================="
  echo "          GKE Project Selection"
  echo "======================================="
  options_project=("Create a new project in Google Cloud" "Use an existing project")
  select opt in "${options_project[@]}"; do
    case $opt in
    "Create a new project in Google Cloud")
         read -r -e -p "Specify Project Name: " PROJECT_ID
         gcloud projects create $PROJECT_ID
         return
        ;;
    "Use an existing project")
        echo "======================================="
        echo "          Choose from the list         "
        echo "======================================="
        PS3="Enter a number: "
        select opt in `gcloud projects list | cut -d " " -f 1 | grep -v PROJECT_ID`;
          do
            PROJECT_ID=$opt
            return
          done
        return
        ;;
    esac
  done
}

create_gke_bucket() {
  echo "============================================="
  echo       "Create a Google Storage Bucket"
  echo "============================================="
  gsutil mb -l ${REGION} gs://$CLIENT_ID
  return
}

create_gke_k8s() {
  echo "============================================="
  echo         "Create a Kubernetes Cluster"
  echo "============================================="
  gcloud beta container --project "$PROJECT_ID" clusters create "$CLIENT_ID" --region "$REGION" --no-enable-basic-auth --release-channel "regular" --machine-type "$GCPINSTANCETYPE" --image-type "COS_CONTAINERD" --disk-type "pd-ssd" --disk-size "192" --metadata disable-legacy-endpoints=true --scopes "https://www.googleapis.com/auth/devstorage.read_write","https://www.googleapis.com/auth/logging.write","https://www.googleapis.com/auth/monitoring","https://www.googleapis.com/auth/servicecontrol","https://www.googleapis.com/auth/service.management.readonly","https://www.googleapis.com/auth/trace.append" --max-pods-per-node "110" --num-nodes "3" --logging=SYSTEM,WORKLOAD --monitoring=SYSTEM --enable-ip-alias --network "projects/$PROJECT_ID/global/networks/default" --subnetwork "projects/$PROJECT_ID/regions/$REGION/subnetworks/default" --no-enable-intra-node-visibility --default-max-pods-per-node "110" --enable-autoscaling --min-nodes "0" --max-nodes "20" --no-enable-master-authorized-networks --addons HorizontalPodAutoscaling,HttpLoadBalancing,GcePersistentDiskCsiDriver --enable-autoupgrade --enable-autorepair --max-surge-upgrade 1 --max-unavailable-upgrade 0 --enable-shielded-nodes --workload-pool="$PROJECT_ID.svc.id.goog" --workload-metadata=GKE_METADATA
  return
}

create_gke_sa() {
  echo "============================================="
  echo   "Create a service account for the cluster"
  echo "============================================="
  gcloud iam service-accounts create hdx-${CLIENT_ID}-sa --project=${PROJECT_ID}
  GCP_STORAGE_SA=hdx-${CLIENT_ID}-sa@${PROJECT_ID}.iam.gserviceaccount.com
  return
}

create_gke_sa_permission() {
  echo "============================================="
  echo "Give service account permission to the bucket"
  echo "============================================="
  gsutil iam ch serviceAccount:${GCP_STORAGE_SA}:roles/storage.objectAdmin gs://$CLIENT_ID
  gcloud iam service-accounts add-iam-policy-binding ${GCP_STORAGE_SA} --role roles/iam.workloadIdentityUser --member "serviceAccount:${PROJECT_ID}.svc.id.goog[${CLIENT_ID}/hydrolix]"
  gcloud iam service-accounts add-iam-policy-binding ${GCP_STORAGE_SA} --role roles/iam.workloadIdentityUser --member "serviceAccount:${PROJECT_ID}.svc.id.goog[${CLIENT_ID}/turbine-api]"
  gcloud iam service-accounts add-iam-policy-binding ${GCP_STORAGE_SA} --role roles/iam.workloadIdentityUser --member "serviceAccount:${PROJECT_ID}.svc.id.goog[${CLIENT_ID}/vector]"
  return
}

install_gke () {
  export USE_GKE_GCLOUD_AUTH_PLUGIN=True
  clear
  echo "======================================="
  echo "            GKE Selected"
  echo "======================================="
  echo "Verifying gcloud, gsutil and kubectl are present"
  verify_command_gke "gcloud"
  verify_command_gke "kubectl"
  verify_command_gke "gsutil"
  create_gke_project
  gcloud config set project $PROJECT_ID
  clear
  verify_gke_right
  get_hydrolix_cluster_info
  select_gke_region
  scale
  SCALE="${SCALE:=prod}"
  download_hkt
  create_gke_bucket
  create_gke_k8s
  create_gke_sa
  create_gke_sa_permission
  create_namespace
  create_hydrolix_cluster "gcp"
  kubectl_loop_gke
  deployment_done "gcp"
  exit
}

################ END #################
###### Specific GKE Function #########
######################################



######################################
##### Specific EKS Function ##########
######################################

select_aws_region() {
PS3="Enter a number: "
select opt in `aws ec2 describe-regions --all-regions --query "Regions[].{Name:RegionName}" --output text`;
do
  REGION=$opt
  return
done
}



verify_command_eks() {
if ! command -v $1 &> /dev/null
then
    echo "$1 could not be found"
    echo "Installation canceled"
    echo "Please follow this guide to setup your environment"
    echo "https://docs.aws.amazon.com/eks/latest/userguide/getting-started.html"
    exit
fi
}



get_oidc_eks() {
  OIDC_PROVIDER="$(aws --region "$REGION" eks describe-cluster --name "$KUBERNETES_CLUSTER" --query "cluster.identity.oidc.issuer" --output text | sed -e "s/^https:\/\///")"
  if [ -z "$OIDC_PROVIDER" ]
  then
      echo "Retrieving Identity for K8s"
      sleep 5
      get_oidc_eks
  else
      return
  fi
}



kubectl_loop_eks() {
  AWSELB=`kubectl get services -o yaml | grep -A 1 "ingress:" | grep hostname | cut -d ":" -f 2 | cut -d " " -f 2`
  if [ -z "$AWSELB" ]
  then
      echo "==========================================="
      echo "Creation of Hydrolix deployment in progress"
      echo "==========================================="
      sleep 30
      kubectl_loop_eks
  elif [ "$AWSELB" == "<pending>" ]
  then
      echo "Creation of Hydrolix deployment in progress"
      sleep 30
      kubectl_loop_eks
  else
      return
fi
}






install_eks () {
  clear
  echo "======================================="
  echo "            EKS Selected"
  echo "======================================="
  echo "Verifying aws, eksctl and kubectl are present"
  verify_command_eks "aws"
  verify_command_eks "kubectl"
  verify_command_eks "eksctl"
  echo "Done"
  echo "======================================="
  echo "        Getting AWS Account ID         "
  echo "======================================="
  AWS_ACCOUNT_ID="$(aws sts get-caller-identity --query "Account" --output text)"
  if [ -z "$AWS_ACCOUNT_ID" ]
  then
      echo "Impossible to retrieve your AWS Account ID"
      echo "Please make sure your AWS CLI works"
      break
  fi
  echo "Creating cluster using AWS Account ID: $AWS_ACCOUNT_ID"
  CLIENT_ID="hdxcli-"`openssl rand -hex 4`
  AWS_STORAGE_ROLE="arn:aws:iam::$AWS_ACCOUNT_ID:role/$CLIENT_ID-bucket"
  echo "======================================="
  echo "Please configure the following settings"
  echo "======================================="
  read -r -e -p "Email adress for user: " ADMIN_EMAIL
  read -r -e -p "Name for admin (defaul admin): " OWNER
  OWNER="${OWNER:=admin}"
  printf "Hostname for Hydrolix cluster (example https://\e[1;33;4;44mhydrolix\e[0m.company.net)"
  read -r -e -p ": " HYDROLIX_HOST
  printf "Domain for Hydrolix cluster (example https://hydrolix.\e[1;33;4;44mcompany.net\e[0m)"
  read -r -e -p ": " HYDROLIX_DOMAIN
  echo "Select region (default us-east-1): "
  select_aws_region
  REGION="${REGION:=us-east-1}"
  read -r -e -p "Kubernetes Cluster Name: " KUBERNETES_CLUSTER
  echo "Select your scale profile (default prod): "
  scale
  SCALE="${OWNER:=prod}"
  echo "============================================="
  echo " Downloading Hydrolix Kubernetes Tools $HKT  "
  echo "============================================="
  curl -o hkt -s https://hdx-infrastructure.s3.amazonaws.com/hkt/hkt-$HKT
  chmod +x hkt
  echo "============================================="
  echo "          Creating an S3 bucket              "
  echo "============================================="
  aws s3 mb --region "$REGION" "s3://$CLIENT_ID"
  echo "============================================="
  echo "           Creating S3 Policy                "
  echo "============================================="
  read -r -d '' POLICY_DOC << EOF
  {
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "ListObjectsInBucket",
            "Effect": "Allow",
            "Action": [
                "s3:ListBucket"
            ],
            "Resource": [
                "arn:aws:s3:::$CLIENT_ID",
                "arn:aws:s3:::hdx-public"
            ]
        },
        {
            "Sid": "AllObjectActions",
            "Effect": "Allow",
            "Action": "s3:*Object",
            "Resource": [
                "arn:aws:s3:::$CLIENT_ID/*",
                "arn:aws:s3:::hdx-public/*"
            ]
        }
    ]
  }
EOF
  aws iam create-policy --policy-name "$CLIENT_ID-bucket" --policy-document "$POLICY_DOC"
  echo "============================================="
  echo "     Creating K8 cluster please be patient   "
  echo "============================================="
  cat > eksctl.yaml << EOF
---
apiVersion: eksctl.io/v1alpha5
kind: ClusterConfig

metadata:
  name: $KUBERNETES_CLUSTER 
  region: $REGION 

addons:
  - name: aws-ebs-csi-driver

iam:
  withOIDC: true

managedNodeGroups:
  - name: nodegroup0 
    instanceType: $AWSINSTANCETYPE
    minSize: 3
    maxSize: 30
    desiredCapacity: $AWSCAPACITY
    volumeSize: 256
    privateNetworking: true
EOF
  eksctl create cluster -f eksctl.yaml && rm -rf eksctl.yaml
  get_oidc_eks
  echo "============================================="
  echo "     Create IAM Policy for Service Account   "
  echo "============================================="
  read -r -d '' SA_POLICY_DOC << EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Federated": "arn:aws:iam::$AWS_ACCOUNT_ID:oidc-provider/$OIDC_PROVIDER"
      },
      "Action": "sts:AssumeRoleWithWebIdentity",
      "Condition": {
        "StringEquals": {
          "$OIDC_PROVIDER:aud": "sts.amazonaws.com",
          "$OIDC_PROVIDER:sub": "system:serviceaccount:$CLIENT_ID:hydrolix"
        }
      }
    },
    {
      "Effect": "Allow",
      "Principal": {
        "Federated": "arn:aws:iam::$AWS_ACCOUNT_ID:oidc-provider/$OIDC_PROVIDER"
      },
      "Action": "sts:AssumeRoleWithWebIdentity",
      "Condition": {
        "StringEquals": {
          "$OIDC_PROVIDER:aud": "sts.amazonaws.com",
          "$OIDC_PROVIDER:sub": "system:serviceaccount:$CLIENT_ID:turbine-api"
        }
      }
    },
    {
      "Effect": "Allow",
      "Principal": {
        "Federated": "arn:aws:iam::$AWS_ACCOUNT_ID:oidc-provider/$OIDC_PROVIDER"
      },
      "Action": "sts:AssumeRoleWithWebIdentity",
      "Condition": {
        "StringEquals": {
          "$OIDC_PROVIDER:aud": "sts.amazonaws.com",
          "$OIDC_PROVIDER:sub": "system:serviceaccount:$CLIENT_ID:vector"
        }
      }
    }
  ]
}
EOF

  aws iam create-role --role-name "$CLIENT_ID-bucket" --assume-role-policy-document "$SA_POLICY_DOC" --description "$CLIENT_ID-bucket"
  aws iam attach-role-policy --role-name "$CLIENT_ID-bucket" --policy-arn="arn:aws:iam::$AWS_ACCOUNT_ID:policy/$CLIENT_ID-bucket"
  AWS_STORAGE_ROLE="arn:aws:iam::$AWS_ACCOUNT_ID:role/$CLIENT_ID-bucket"
  echo "============================================="
  echo "     Setup GP3 High Performance Disk         "
  echo "============================================="
  cat > gp3.yaml << EOF
---
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: gp3
provisioner: ebs.csi.aws.com
parameters:
  type: gp3
reclaimPolicy: Retain
volumeBindingMode: WaitForFirstConsumer
EOF

kubectl apply -f gp3.yaml && rm -rf gp3.yaml
  echo "============================================="
  echo "             Setup autoscaling               "
  echo "============================================="
  kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/latest/download/components.yaml
  read -r -d '' AUTOSCALER_POLICY_DOC << EOF
{   
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "autoscaling:SetDesiredCapacity",
                "autoscaling:TerminateInstanceInAutoScalingGroup"
            ],
            "Resource": "*",
            "Condition": {
                "StringEquals": {
                    "aws:ResourceTag/k8s.io/cluster-autoscaler/ci": "owned"
                }
            }
        },
        {
            "Sid": "VisualEditor1",
            "Effect": "Allow",
            "Action": [
                "autoscaling:DescribeAutoScalingInstances",
                "autoscaling:DescribeAutoScalingGroups",
                "ec2:DescribeLaunchTemplateVersions",
                "autoscaling:DescribeTags",
                "autoscaling:DescribeLaunchConfigurations"
            ],
            "Resource": "*"
        }
    ]
}
EOF
  aws iam create-policy --policy-name eks-${KUBERNETES_CLUSTER}-autoscaler --policy-document "$AUTOSCALER_POLICY_DOC"
  read -r -d '' AUTOSCALER_TRUST_DOC << EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Federated": "arn:aws:iam::$AWS_ACCOUNT_ID:oidc-provider/$OIDC_PROVIDER"
      },
      "Action": "sts:AssumeRoleWithWebIdentity",
      "Condition": {
        "StringEquals": {
          "$OIDC_PROVIDER:aud": "sts.amazonaws.com",
          "$OIDC_PROVIDER:sub": "system:serviceaccount:kube-system:cluster-autoscaler"
        }
      }
    }
  ]
}
EOF
  aws iam create-role --role-name "eks-${KUBERNETES_CLUSTER}-autoscaler" --assume-role-policy-document "$AUTOSCALER_TRUST_DOC" --description "eks-${KUBERNETES_CLUSTER}-autoscaler"
  aws iam attach-role-policy --role-name "eks-${KUBERNETES_CLUSTER}-autoscaler" --policy-arn="arn:aws:iam::$AWS_ACCOUNT_ID:policy/eks-${KUBERNETES_CLUSTER}-autoscaler"
  curl -s -o cluster-autoscaler-autodiscover.yaml https://raw.githubusercontent.com/kubernetes/autoscaler/master/cluster-autoscaler/cloudprovider/aws/examples/cluster-autoscaler-autodiscover.yaml
  sed "s/<YOUR CLUSTER NAME>/${KUBERNETES_CLUSTER}/" cluster-autoscaler-autodiscover.yaml > cluster-autoscale-autodiscover-mod.yaml
  kubectl apply -f cluster-autoscale-autodiscover-mod.yaml
  rm cluster-autoscaler-autodiscover.yaml
  rm cluster-autoscale-autodiscover-mod.yaml
  kubectl annotate serviceaccount cluster-autoscaler -n kube-system eks.amazonaws.com/role-arn=arn:aws:iam::${AWS_ACCOUNT_ID}:role/eks-${KUBERNETES_CLUSTER}-autoscaler
  create_namespace
  create_hydrolix_cluster "aws"
  kubectl_loop_eks
  deployment_done "aws"
  exit
}

################ END #################
###### Specific EKS Function #########
######################################

######################################
############ Main Prompt #############
######################################


PS3='Please enter your choice: '
clear

echo "==========================="
echo "     Hydrolix Setup"
echo "==========================="

options=("Install on Google Cloud with GKE" "Install on AWS with EKS" "Quit")

COLUMNS=12


select opt in "${options[@]}"; do
    case $opt in

    "Install on Google Cloud with GKE")
        install_gke
        ;;
    "Install on AWS with EKS")
        install_eks
        ;;
    "Quit")
        exit
        ;;
    esac
done
