#!/usr/bin/python3
import csv
import json
import requests

mapping = input("Please enter the raw csv URL for the mapping example if empty we'll use: https://raw.githubusercontent.com/elastic/ecs/1.10/generated/csv/fields.csv\r\n")
tableid = input("Please enter your tableid generated in our portal: ")

if mapping:
    url = mapping
else:
    url = "https://raw.githubusercontent.com/elastic/ecs/1.10/generated/csv/fields.csv"

r = requests.get(url)
if (r.status_code == 200):
    f = open("fields.csv", "w")
    f.write(r.text)
    f.close()
    with open('fields.csv', newline='') as csvfile:
        global transform
        global error
        error = []
        transform = []
        reader = csv.DictReader(csvfile)
        for row in reader:
            ## Define @timestamp as primary key
            if (row['Type'] == "date" and row['Field'] == "@timestamp"):
                transform.append({ "name": row['Field'], "datatype": {"primary": "true", "index": "true", "resolution": "milliseconds", "type": "datetime", "format": "2006-01-02T15:04:05.999Z0700"}})
            
            ## Check if data is an array and generate Hydrolix array for different data type
            if (row['Normalization'] == "array"):
                if (row['Type'] == "keyword"):
                    transform.append({ "name": row['Field'], "datatype": {"type": "array", "elements": [{"index": "true", "type": "string"}]}})

                elif (row['Type'] == "object"):
                    transform.append({ "name": row['Field'], "datatype": {"type": "array", "elements": [{"index": "true", "type": "string"}]}})

                elif (row['Type'] == "flattened"):
                    transform.append({ "name": row['Field'], "datatype": {"type": "array", "elements": [{"index": "true", "type": "string"}]}})

                elif (row['Type'] == "nested"):
                    transform.append({ "name": row['Field'], "datatype": {"type": "array", "elements": [{"index": "true", "type": "string"}]}})

                elif (row['Type'] == "constant_keyword"):
                    transform.append({ "name": row['Field'], "datatype": {"type": "array", "elements": [{"index": "true", "type": "string"}]}})

                elif (row['Type'] == "text"):
                    transform.append({ "name": row['Field'], "datatype": {"type": "array", "elements": [{"index": "true", "type": "string"}]}})

                elif (row['Type'] == "ip"):
                    transform.append({ "name": row['Field'], "datatype": {"type": "array", "elements": [{"index": "true", "type": "string"}]}})

                elif (row['Type'] == "long"):
                    transform.append({ "name": row['Field'], "datatype": {"type": "array", "elements": [{"type": "int64"}]}})

                elif (row['Type'] == "float"):
                    transform.append({ "name": row['Field'], "datatype": {"type": "array", "elements": [{"type": "int32"}]}})

                elif (row['Type'] == "scaled_float"):
                    transform.append({ "name": row['Field'], "datatype": {"type": "array", "elements": [{"type": "int64"}]}})

                elif (row['Type'] == "integer"):
                    transform.append({ "name": row['Field'], "datatype": {"type": "array", "elements": [{"type": "int32"}]}})

                elif (row['Type'] == "double"):
                    transform.append({ "name": row['Field'], "datatype": {"type": "array", "elements": [{"type": "int64"}]}})

                elif (row['Type'] == "boolean"):
                    transform.append({ "name": row['Field'], "datatype": {"type": "array", "elements": [{"type": "boolean"}]}})

                elif (row['Type'] == "date"):
                    transform.append({ "name": row['Field'], "datatype": {"type": "array", "elements": [{"index": "true", "resolution": "milliseconds", "type": "datetime", "format": "2006-01-02T15:04:05.999Z0700"}]}})

                ## If unknown data type generator error
                else:
                    error.append({"Error unknown data type" : row['Type'], "For field": row['Field']})

            ## Data is not an array, generate transform for the different type
            elif (row['Normalization'] != "array"):

                if (row['Type'] == "keyword"):
                    transform.append({ "name": row['Field'], "datatype": {"index": "true","type": "string"}})

                elif (row['Type'] == "object"):
                    transform.append({ "name": row['Field'], "datatype": {"index": "true","type": "string"}})

                elif (row['Type'] == "constant_keyword"):
                    transform.append({ "name": row['Field'], "datatype": {"index": "true","type": "string"}})

                elif (row['Type'] == "text"):
                    transform.append({ "name": row['Field'], "datatype": {"index": "true","type": "string"}})

                elif (row['Type'] == "geo_point"):
                    transform.append({ "name": row['Field'] + ".lat", "datatype": {"type": "int32"}})
                    transform.append({ "name": row['Field'] + ".lon", "datatype": {"type": "int32"}})

                elif (row['Type'] == "ip"):
                    transform.append({ "name": row['Field'], "datatype": {"index": "true","type": "string"}})

                elif (row['Type'] == "long"):
                    transform.append({ "name": row['Field'], "datatype": {"type": "int64"}})

                elif (row['Type'] == "float"):
                    transform.append({ "name": row['Field'], "datatype": {"type": "int32"}})

                elif (row['Type'] == "scaled_float"):
                    transform.append({ "name": row['Field'], "datatype": {"type": "int64"}})

                elif (row['Type'] == "boolean"):
                    transform.append({ "name": row['Field'], "datatype": {"type": "boolean"}})

                elif (row['Type'] == "integer"):
                    transform.append({ "name": row['Field'], "datatype": {"type": "int32"}})

                elif (row['Type'] == "half_float"):
                    transform.append({ "name": row['Field'], "datatype": {"type": "int64"}})

                elif (row['Type'] == "date" and row['Field'] != "@timestamp"):
                    transform.append({ "name": row['Field'], "datatype": {"index": "true", "resolution": "milliseconds", "type": "datetime", "format": "2006-01-02T15:04:05.999Z0700"}})

                ## If unknown data type generator error
                else:
                    error.append({"Error unknown data type" : row['Type'], "For field": row['Field']})

        if (len(error) > 1):
            print(json.dumps(error, indent=2))
        else:
            print(json.dumps({"name": "ecs_transform", "type": "json", "table" : tableid, "settings": { "is_default": "true", "output_columns": transform, "compression": "none", "format_details": {"flattening": {"active": "true", "map_flattening_strategy": {"left" : "."}}}}}, indent=2))

else:
    print("Unable to retrieve ECS from Github")
